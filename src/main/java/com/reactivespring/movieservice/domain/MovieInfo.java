package com.reactivespring.movieservice.domain;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;
import java.time.LocalDate;
import java.util.List;

@Data // Getters and Setters
@NoArgsConstructor
@AllArgsConstructor
public class MovieInfo {

    private String movieInfoId;
    @NotBlank(message = "Movie name must be present")
    private String name;
    @NotNull
    @Positive(message = "Year must be a positive value")
    private Integer year;
    private String description;
    private List<@NotBlank(message = "Cast must be present")String> cast;
    private LocalDate releaseDate;
}
