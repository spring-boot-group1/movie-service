package com.reactivespring.movieservice.client;

import com.reactivespring.movieservice.domain.MovieInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.core.publisher.Mono;

@Component
public class MovieInfoRestClient {

    @Autowired
    private WebClient.Builder webClient;

    @Value("${restClient.moviesInfoUrl}")
    private String moviesInfoUrl;

    public Mono<MovieInfo> retrieveMovieInfo(String movieId){
        var url = moviesInfoUrl.concat("/{id}");
        return webClient.build()
                .get()
                .uri(url,movieId)
                .retrieve()
                .bodyToMono(MovieInfo.class)
                .log();
    }
}
