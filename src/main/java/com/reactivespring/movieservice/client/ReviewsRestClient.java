package com.reactivespring.movieservice.client;

import com.reactivespring.movieservice.domain.Review;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.web.reactive.function.client.WebClient;
import org.springframework.web.util.UriComponentsBuilder;
import reactor.core.publisher.Flux;

@Component
public class ReviewsRestClient {

    @Autowired
    private WebClient.Builder webClient;

    @Value("${restClient.reviewsUrl}")
    private String reviewsUrl;

    public Flux<Review> retriveReviews(String movieId){

        String url = UriComponentsBuilder.fromHttpUrl(reviewsUrl)
                        .queryParam("movieInfoId", movieId)
                        .buildAndExpand().toString();
        return webClient.build()
                    .get()
                    .uri(url)
                    .retrieve()
                    .bodyToFlux(Review.class)
                    .log();
    }
}
