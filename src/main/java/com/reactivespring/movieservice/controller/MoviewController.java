package com.reactivespring.movieservice.controller;

import com.reactivespring.movieservice.client.MovieInfoRestClient;
import com.reactivespring.movieservice.client.ReviewsRestClient;
import com.reactivespring.movieservice.domain.Movie;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Mono;

@RestController
@RequestMapping("/v1/movie")
public class MoviewController {

    @Autowired
    private MovieInfoRestClient movieInfoRestClient;

    @Autowired
    private ReviewsRestClient reviewsRestClient;

    @GetMapping("/{id}")
    public Mono<Movie> getMovieById(@PathVariable("id") String movieId){

        return movieInfoRestClient.retrieveMovieInfo(movieId)
                .flatMap(movieInfo -> {
                    var reviewListMono = reviewsRestClient.retriveReviews(movieId).collectList();
                    return reviewListMono.map(reviews -> new Movie(movieInfo,reviews));
                });

    }

}
